#!/bin/bash
apt-get install -y heartbeat
cp /vagrant/etc/ha.d/ha.cf /etc/ha.d/ha.cf
cp /vagrant/etc/ha.d/haresources /etc/ha.d/haresources
cp /vagrant/etc/ha.d/authkeys /etc/ha.d/authkeys
chmod 600 /etc/ha.d/authkeys
bash /vagrant/update_ip.sh
/etc/init.d/heartbeat start
